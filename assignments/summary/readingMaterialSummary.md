# **IoT Basics**

# **Industrial Revolution**

![](https://sc-cms-web.azureedge.net/-/media/images/aerotek/business-insights/aerotek-infographic-revolution-of-industial-change-jpg.jpg?h=300&la=en&w=600&rev=491a0590737c49669d128bd6b2d7a562&hash=925FCC976C1CF23D4AFFAFC189D7FE42)

# **Industry 3.0 hierarchy and architecture**


- Data is stored in databases and represented in excels.                                        

![](https://www.i40.ovgu.de/i40_media/I40_Medien/031-height-668-width-1189.png)

1. **Field Devices** - They are at the bottom of the Hierarchy. These are the sensors or actuators present in the factory, they    measuring things or actuate machines (doing some actions).

1. **Control Devies** - These are the PCs , PLCs etc. They tell the field devices what to do and control them.
   Microcontrollers, Computer Numeric Controls.
 
1. **Station** - A set of Field devices & Control Devices together is called a station.

1. **Work Centres** - A set of stations together is called a work centre (Factories). They use softwares like Scada & Historian.

1. **Enterprise** - A set of Work Centres together becomes an Enterprise. They use softwares like ERP & MES.

## **Industry 3.0 Protocols**

1. Modbus
1. Profi-Network
1. CANopen
1. EtherCAT

# **Industry 4.0**

![](https://aethon.com/wp-content/uploads/2015/07/Industry4.0.png)

It is industry 3.0 connected to the internet.
Now we can remotely access the data recieved because of the industries connected to the internet.

## **Industry 4.0 Architecture**
Now the data recieved is sent to the cloud.
Some of the protocols used by the industry 4.0 are

- MQTT
- Web Sockets
- https
- REST API
- AMQP
- CoAP

All these protocols are optimized to send data to the cloud for data analysis

## **Industry 3.0 to Industry 4.0**

**Problems Faced with Industry 4.0 upgradation**

- Cost.
- Downtime.
- Reliablity.

Parameter need to overcome from  Conversion From 3.0 to 4.0

- Requirement of new skill staff
- New hardware expense
- Time requied for install
- Lack of Documentation
- Properitary PLC prtocol

## **HOW TO CONVERT FROM 3.0 to 4.0**

We can use a library called Shunya Interfaces. It can used on an embedded system like raspberry pi and use it as per our requirements.

- Step 1: Identifying the most popular Industry 3.0 devices.
- Step 2: Study Protocols that these devices communicate.
- Step 3: Get data From the industry 3.0 devices.
- Step 4: Send the data to the cloud for industry 4.0
- After the data is sent to the internet/cloud, we can

**Store data in Time Shared Data database(TSDB) using tools like**

-  Prometheus
-  InfluxDB
-  Things Board
-  Grafana(Analytics tool for analysis)

**Make a Dashboard of the datas recieved using tools like**

- Grafana
- Thingsboard
- Cayenne

**Analyse Data using platforms like**

- AWS IoT
- Google IoT
- Azure IoT
- Thingsboard

**Gets Alerts using platforms like**

- Zaiper
- Twilio
- Integromat
- IFTTT





